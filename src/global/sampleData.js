import { media } from "../assets/media"


export const home_categories = [
    {
        title: 'Clothes',
        icon: media.clothes,
    },
    {
        title: 'Shoes',
        icon: media.shoes,
    },
    {
        title: 'Bags',
        icon: media.bags,
    },
]

export const product_categories = [
    {
        title: 'All',
    },
    {
        title: 'Clothes',
    },
    {
        title: 'Shoes',
    },
    {
        title: 'Bags',
    },
]


export const carousel_data = [
    { 
        id: '1', 
        discount: '30%',
        title: 'Today’s Special!',
        subTitle: 'Get discount for every order, only valid for today',
        image: media.carousel_img 
    },
    { 
        id: '2', 
        discount: '25%',
        title: 'Today’s Special!',
        subTitle: 'Get discount for every order, only valid for today',
        image: media.carousel_img 
    },
    { 
        id: '3', 
        discount: '45%',
        title: 'Today’s Special!',
        subTitle: 'Get discount for every order, only valid for today',
        image: media.carousel_img 
    },
  ];

export const product_data = [
    {
        name: 'U.S. Polo Assn.',
        description: 'Pure Cotton Slim Fit T-shirt',
        price: '$10.99',
        image: media.product_1
    },
    {
        name: 'Nautica',
        description: 'Men Slim Fit Cargo Shorts',
        price: '$15.99',
        image: media.product_2
    },
    {
        name: 'AASK',
        description: 'Striped Midi Fit and Flare Dresses',
        price: '$20.99',
        image: media.product_3
    },
    {
        name: 'Puma',
        description: 'Unisex Black Solid teamGOAL 23 Backpack',
        price: '$25.99',
        image: media.product_4
    },
    {
        name: 'Wildcraft',
        description: 'Unisex Black Solid Backpack',
        price: '$30.99',
        image: media.product_5
    },
    {
        name: 'Sangria',
        description: 'Floral Kantha Jaal Printed Flounce Cotton A-Line Dresses',
        price: '$35.99',
        image: media.product_6
    },
    {
        name: 'Marks & Spencer',
        description: 'Men Mid-Rise Regular Shorts',
        price: '$40.99',
        image: media.product_7
    },
    {
        name: 'AMERICAN EAGLE OUTFITTERS',
        description: 'Men Mid-Rise Distressed Denim Shorts',
        price: '$45.99',
        image: media.product_8
    },
    {
        name: 'AAHWAN',
        description: 'Woman Solid Square Neck Fitted Crop Top',
        price: '$50.99',
        image: media.product_9
    },
    {
        name: 'CINOCCI',
        description: 'Men Washed Slim Fit Distressed Denim Shorts',
        price: '$55.99',
        image: media.product_10
    }
  ];
  