import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import WalletScreen from '../../screens/wallet';

const Stack = createStackNavigator();

const WalletStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Wallet" 
        >
            <Stack.Screen
                name="Wallet"
                component={WalletScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default WalletStack