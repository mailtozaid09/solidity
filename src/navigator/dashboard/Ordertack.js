import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import OrderScreen from '../../screens/order';

const Stack = createStackNavigator();

const OrderStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Order" 
        >
            <Stack.Screen
                name="Order"
                component={OrderScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default OrderStack