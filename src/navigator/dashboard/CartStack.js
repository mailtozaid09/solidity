import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import CartScreen from '../../screens/cart';

const Stack = createStackNavigator();

const CartStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Cart" 
        >
            <Stack.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default CartStack