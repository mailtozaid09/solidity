import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/home';

const Stack = createStackNavigator();

const HomeStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default HomeStack