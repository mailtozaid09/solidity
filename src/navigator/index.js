import React, {useState, useEffect, useContext} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import Tabbar from './tabbar';

const Stack = createStackNavigator();

const Navigator = ({}) => {

    useEffect(() => {
        console.log("Navigator > > ");
    }, [])
    

    return (
        <>
            <Stack.Navigator 
            initialRouteName={'Tabbar'}  
            >
                <Stack.Screen
                    name="Tabbar"
                    component={Tabbar}
                    options={{
                        headerShown: false
                    }}
                />
            </Stack.Navigator>
        </>
    );
}

export default Navigator