import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'

import { colors } from '../../utils'

import { HomeHeader } from '../../components/headers'

import Input from '../../components/input'
import CardTitle from '../../components/CardTitle'
import ProductList from '../../components/ProductList'
import HomeCategories from '../../components/HomeCategories'
import CarouselWithPagination from '../../components/Carousel'



const HomeScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <HomeHeader
                title={"Zaid Ahmed"}
                headerMsg="Good Morning"
            />
            <ScrollView>
                <View style={styles.mainContainer} >

                    <Input
                        isSearch
                        placeholder="Search"
                    />

                    <CardTitle
                        title="Special Offers"
                        isSeeAll={true}
                        onPress={() => {}}
                    />

                    <CarouselWithPagination />

                    <HomeCategories />

                    <ProductList />

                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        padding: 20,
        paddingTop: 0,
    }
})

export default HomeScreen