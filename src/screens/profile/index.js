import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../utils'
import AppText from '../../components/text'
import { CustomHeader } from '../../components/headers'

const ProfileScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader title={"Profile"} />
            <View style={styles.mainContainer} >
                <AppText
                    text={'ProfileScreen'}
                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                    customStyle={{textAlign: 'center', }}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default ProfileScreen