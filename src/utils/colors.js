export default colors = {
    primary: '#FC6C85',
    light_primary: '#FC6C8550',
    

    bg_color: '#fbfbfb',
    black: '#282828',
    white: '#fff',

    gray: '#696969',
    light_gray: '#ebecf0',
    dark_gray: '#3d3d3d',
    disabled: '#C2C2C2',

    green: '#007c02',
    dark_gray: '#3d3d3d',
    yellow: '#ffb32d',

    brown: '#EA9F5F',
    red: '#FF0000',

    cream: '#E8B88D',
    light_cream: '#F0E3D5',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    

    
    
    
    blue: '#81b3f3',

    orange: '#f7c191',
    reddish: '#ED4545',

}