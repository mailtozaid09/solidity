export default STRING_CONSTANTS = {
    textConstants: {
        LABEL: 'LABEL',
    
        LARGE_TITLE: 'LARGE_TITLE',
        TITLE1: 'TITLE1',
        TITLE2: 'TITLE2',
        TITLE3: 'TITLE3',
    
        HEADING: 'HEADING',
        HEADLINE1: 'HEADLINE1',
        HEADLINE2: 'HEADLINE2',
        HEADLINE3: 'HEADLINE3',
    
        SUBHEADLINE1: 'SUBHEADLINE1',
        SUBHEADLINE2: 'SUBHEADLINE2',
        SUBHEADLINE3: 'SUBHEADLINE3',
    
    
    
        SUBHEAD: 'SUBHEAD',
        CALLOUT: 'CALLOUT',
      
        CAPTION1: 'CAPTION1',
        CAPTION2: 'CAPTION2',
        CAPTION3: 'CAPTION3',
    
        BODY: 'BODY',
        TINY: 'TINY',

        ERROR: 'ERROR',
    },
}

