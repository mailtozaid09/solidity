
const primary_font = 'Poppins'
const secondary_font = 'Poppins'

const google_font = 'GoogleSans'

export default fonts =  {
    google_bold_font: `${google_font}-Bold`,
    google_medium_font: `${google_font}-Medium`,
    google_regular_font: `${google_font}-Regular`,
    
    primary_bold_font: `${primary_font}-Bold`,
    primary_semi_bold_font: `${primary_font}-SemiBold`, 
    primary_medium_font: `${primary_font}-Medium`,
    primary_regular_font: `${primary_font}-Regular`,
    primary_light_font: `${primary_font}-Light`,
    primary_thin_font: `${primary_font}-Thin`,

    // secondary_bold_font: `${secondary_font}-Bold`,
    // secondary_semi_bold_font: `${secondary_font}-SemiBold`, 
    // secondary_medium_font: `${secondary_font}-Medium`,
    // secondary_regular_font: `${secondary_font}-Regular`,
    // secondary_light_font: `${secondary_font}-Light`,
    // secondary_thin_font: `${secondary_font}-Thin`,
}
