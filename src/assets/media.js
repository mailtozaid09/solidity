export const media = {
    home_fill: require('../assets/icons/tabbar/home_fill.png'),
    admin_fill: require('../assets/icons/tabbar/admin_fill.png'),
    order_fill: require('../assets/icons/tabbar/order_fill.png'),
    cart_fill: require('../assets/icons/tabbar/cart_fill.png'),
    wallet_fill: require('../assets/icons/tabbar/wallet_fill.png'),

    home: require('../assets/icons/tabbar/home.png'),
    admin: require('../assets/icons/tabbar/admin.png'),
    order: require('../assets/icons/tabbar/order.png'),
    cart: require('../assets/icons/tabbar/cart.png'),
    wallet: require('../assets/icons/tabbar/wallet.png'),

    dummy_avatar: require('../assets/images/dummy_avatar.png'),

    clothes: require('../assets/images/clothes.png'),
    shoes: require('../assets/images/shoes.png'),
    bags: require('../assets/images/bags.png'),

    filter: require('../assets/images/filter.png'),
    search: require('../assets/images/search.png'),

    carousel_img: require('../assets/images/carousel_img.png'),

    //products
    product_1: require('../assets/images/products/product_1.png'),
    product_2: require('../assets/images/products/product_2.png'),
    product_3: require('../assets/images/products/product_3.png'),
    product_4: require('../assets/images/products/product_4.png'),
    product_5: require('../assets/images/products/product_5.png'),
    product_6: require('../assets/images/products/product_6.png'),
    product_7: require('../assets/images/products/product_7.png'),
    product_8: require('../assets/images/products/product_8.png'),
    product_9: require('../assets/images/products/product_9.png'),
    product_10: require('../assets/images/products/product_10.png'),
}
