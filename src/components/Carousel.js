import React, { useState, useRef } from 'react';
import { View, Text, Image, StyleSheet, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { media } from '../assets/media';
import { carousel_data } from '../global/sampleData';
import { STRING_CONSTANTS, colors } from '../utils';
import AppText from './text';
import { screenWidth } from '../global/constants';

const CarouselWithPagination = () => {
    const [activeSlide, setActiveSlide] = useState(0);
    const carouselRef = useRef(null);
  
    const renderItem = ({ item, index }) => (
        <View key={index} style={[styles.slide, { width : screenWidth-40 }]}>
            <View style={styles.contentContainer}>
                <AppText
                    text={item?.discount}
                    type={STRING_CONSTANTS.textConstants.HEADING}
                    customStyle={{color: colors.primary}}
                />
                <AppText
                    text={item?.title}
                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                    customStyle={{marginBottom: 4, color: colors.primary}}
                />
                <AppText
                    text={item?.subTitle}
                    type={STRING_CONSTANTS.textConstants.CAPTION3}
                    customStyle={{color: colors.primary}}
                />
            </View>
            <Image source={item.image} style={styles.image} />
        </View>
    );
  
    const handlePaginationDotPress = (index) => {
        carouselRef.current.scrollToIndex({ index });
        setActiveSlide(index);
    };
  
    return (
        <View style={styles.container}>
            <FlatList
                ref={carouselRef}
                data={carousel_data}
                renderItem={renderItem}
                horizontal
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => item.id}
                onScroll={(event) => {
                    const slideWidth = screenWidth-40;
                    const currentOffset = event.nativeEvent.contentOffset.x;
  
                    const newActiveSlide = Math.floor(currentOffset / slideWidth);

                    setActiveSlide(newActiveSlide);
                }}
            />
            <View style={styles.paginationContainer}>
                {carousel_data.map((_, index) => (
                    <TouchableOpacity
                        key={index}
                        style={[styles.dot, index === activeSlide && styles.activeDot]}
                        onPress={() => handlePaginationDotPress(index)}
                    />
                ))}
            </View>
        </View>
    );
};
  
  const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
    },
    slide: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        borderRadius: 20,
        borderWidth: 1, 
        borderColor: colors.primary,
        backgroundColor: colors.light_primary,
    },
    contentContainer: {
        flex: 1,
    },
    content: {
        fontSize: 18,
    },
    image: {
        width: 150,
        height: 150,
    },
    paginationContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 10,
        left: 0,
        right: 0,
    },
    dot: {
        width: 5,
        height: 5,
        borderRadius: 4,
        backgroundColor: colors.white,
        marginHorizontal: 5,
    },
    activeDot: {
        width: 16,
        height: 5,
        backgroundColor: colors.primary,
    },
  });
  
export default CarouselWithPagination;
