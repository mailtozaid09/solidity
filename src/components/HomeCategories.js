import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { STRING_CONSTANTS, colors } from '../utils'
import Icon from '../utils/icons'
import AppText from './text'
import { home_categories } from '../global/sampleData'


const HomeCategories = ({ title, isSeeAll, onPress, }) => {
    return (
        <View style={styles.container} >
            <FlatList
                data={home_categories}
                keyExtractor={item => item.title}
                contentContainerStyle={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}
                renderItem={({item, index}) => (
                    <View style={{alignItems: 'center', marginVertical: 10}} >
                        <View style={styles.iconContainer} >
                            <Image source={item?.icon} style={styles.icon} />
                        </View>
                        <AppText
                            text={item?.title}
                            type={STRING_CONSTANTS.textConstants.CAPTION2}
                            customStyle={{textAlign: 'center', color: colors.black}}
                        />
            
                    </View>
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 10,
    },
    iconContainer: {
        height: 65,
        width: 65,
        borderRadius: 36,
        marginBottom: 4,
        backgroundColor: colors.light_primary,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        height: 28,
        width: 28,
        resizeMode: 'contain'
    }
})

export default HomeCategories