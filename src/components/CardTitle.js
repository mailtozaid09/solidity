import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity,} from 'react-native'
import { STRING_CONSTANTS, colors } from '../utils'
import Icon from '../utils/icons'
import AppText from './text'


const CardTitle = ({ title, isSeeAll, onPress, }) => {
    return (
        <View style={styles.container} >
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.CAPTION1}
                customStyle={{textAlign: 'center', color: colors.black}}
            />
            
            {isSeeAll && (
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={onPress}
                    
                >
                    <AppText
                        text={'See all'}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{textAlign: 'center', color: colors.primary}}
                    />
                </TouchableOpacity>
            )}
            
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    }
})

export default CardTitle