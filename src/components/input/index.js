import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

import { media } from '../../assets/media';
import { colors, fonts } from '../../utils';
import Icon from '../../utils/icons';



const Input = ({value, label, customStyle, customInputStyle, isLight, placeholder, isPhoneNumber, error, editable, keyboardType, onChangeEyeIcon, showEyeIcon, onChangeText, clearTextValue, isPassword, isSearch }) => {
    return (
        <View style={[styles.container, customStyle]} >
            {label && <Text style={styles.inputLabel} >{label}</Text>}

            <View style={[styles.inputContainer, editable == false && {backgroundColor: colors.light_gray}]} >
            
                {isSearch && (
                    <View>
                          <Image source={media.search} style={{height: 30, width: 30}} />
                    </View>
                )}

                <TextInput
                    value={value}
                    placeholder={placeholder}
                    placeholderTextColor={'#B1B1B1'}
                    onChangeText={onChangeText}
                    style={[styles.input, customInputStyle]}
                    autoCorrect={false}
                    editable={editable}
                    autoCapitalize={label == 'Email' ? 'none' : null}
                    maxLength={isPhoneNumber ? 10 : null}
                    keyboardType={keyboardType ? keyboardType : isPhoneNumber ? 'number-pad' : 'default'}
                    secureTextEntry={isPassword && !showEyeIcon ? true : false}
                />

                {isPassword && (
                    <TouchableOpacity onPress={onChangeEyeIcon}>
                        <Image source={showEyeIcon ? media.view : media.hide} style={styles.iconImage} />
                    </TouchableOpacity>
                )}


                {isSearch && (
                    <TouchableOpacity >
                        <Image source={media.filter} style={{height: 24, width: 24}} />
                    </TouchableOpacity>
                )}
            </View>

            {error && <Text style={styles.error} >{error ? error : null}</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //width: '100%',
        //flex: 1,
        marginBottom: 20,
    },
    input: {
        fontSize: 16,
        lineHeight: 20,
        height: 50,
        fontFamily: fonts.primary_medium_font,
        flex: 1,
        marginRight: 10,
        color: colors.black,
    },
    inputContainer: {
        borderRadius: 10,
        flex: 1,
        //width: '100%', 
        height: 50,       
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        borderRadius: 10,
        borderWidth: 1,
        backgroundColor: '#FAFAFA',
        borderColor: '#EEEEEE',
    },
    inputLabel: {
        fontSize: 14,
        marginBottom: 4,
        color: colors.black,
        fontFamily: fonts.primary_semi_bold_font,
    },
    iconImage: {
        height: 26,
        width: 26
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 6,
        fontFamily: fonts.primary_medium_font,
        color: colors.red
    },

})

export default Input