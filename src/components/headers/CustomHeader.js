import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'

const CustomHeader = ({title, isBack, headerColor, backButtonColor}) => {
    return (
        <View style={[styles.container, isBack ? styles.spaceBetween : styles.center]} >
            {isBack && (<View>
                <BackButton backButtonColor={backButtonColor} />
            </View>)}
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                customStyle={{textAlign: 'center', color: headerColor ? headerColor : colors.black}}
            />
            <View style={{width: 40}} >
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    spaceBetween: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default CustomHeader