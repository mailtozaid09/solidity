import React from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'
import CustomIcon from '../CustomIcon'
import { useNavigation } from '@react-navigation/native'
import { media } from '../../assets/media'

const HomeHeader = ({title, headerMsg, onPress, }) => {

    const navigation = useNavigation()


    return (
        <View style={styles.container} >
            <View style={styles.profileContainer} >
                <Image source={media.dummy_avatar} style={styles.profile} />
                <View>
                    <AppText
                        text={headerMsg}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{}}
                    />
                    <AppText
                        text={title}
                        type={STRING_CONSTANTS.textConstants.CAPTION1}
                        customStyle={{lineHeight: 18}}
                    />
                </View>
            </View>
            <View style={styles.iconContainer} >
                <CustomIcon
                    iconName="notifications-outline"
                    iconType="Ionicons"
                    iconColor={colors.gray}
                    iconContainer={{marginLeft: 14}}
                    iconSize={26}
                    onPress={() => {}}
                />

                <CustomIcon
                    iconName="hearto"
                    iconType="AntDesign"
                    iconColor={colors.gray}
                    iconSize={22}
                    iconContainer={{marginLeft: 14,}}
                    onPress={() => {}}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    profileContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profile: {
        height: 40,
        width: 40,
        marginRight: 10
    }
})

export default HomeHeader