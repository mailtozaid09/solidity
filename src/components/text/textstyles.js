import React from 'react'
import { StyleSheet } from 'react-native'
import { colors, fonts } from '../../utils'

export const textstyles = StyleSheet.create({
    labelStyle: {
        fontSize: 14,
        marginBottom: 4,
        color: colors.black,
        fontFamily: fonts.primary_semi_bold_font,
    },
    heading: {
        fontFamily: fonts.primary_bold_font,
        fontSize: 24,
        fontWeight: '400',
        color: colors.black,
        lineHeight: 34,
    },
    headline1: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 18,
        fontWeight: '500',
        color: colors.black,
    },
    headline2: {
        fontFamily: fonts.primary_semi_bold_font,
        fontSize: 18,
        fontWeight: '700',
        color: colors.black,
    },
    headline3: {
        fontFamily: fonts.primary_bold_font,
        fontSize: 16,
        lineHeight: 20,
        fontWeight: '700',
        color: colors.black,
    },
    subHeadline1: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 18,
        fontWeight: '500',
        color: colors.black,
    },
    subHeadline2: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 18,
        fontWeight: '700',
        color: colors.black,
    },
    subHeadline3: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 16,
        lineHeight: 20,
        fontWeight: '700',
        color: colors.black,
    },
    
    caption1: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 16,
        fontWeight: '400',
        color: colors.grey,
    },
    caption2: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 14,
        fontWeight: '400',
        color: colors.grey,
    },
    caption3: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 11,
        fontWeight: '500',
        color: colors.grey,
    },


    defaultStyles: {
        color: colors.red,
        fontFamily: fonts.primary_medium_font,
    },
    
    largeTitle: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 34,
        fontWeight: '500',
        letterSpacing: -0.5,
    },
    title1: {
        fontFamily: fonts.primary_bold_font,
        fontSize: 16,
        fontWeight: '500',
        lineHeight: 20,
        color: colors.black,
    },
    title2: {
        fontFamily: fonts.primary_semi_bold_font,
        fontSize: 16,
        lineHeight: 20,
        fontWeight: 'bold',
        color: colors.black,
    },
    title3: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 14,
        lineHeight: 18,
        fontWeight: '500',
        color: colors.black,
    },
    
    subhead: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.1,
    },

    error: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 14,
        lineHeight: 18,
        fontWeight: '400',
        marginTop: 6,
        color: colors.red
    },
    body: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 14,
        fontWeight: '400',
        color: colors.black,
    },
    tiny: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 11,
        fontWeight: '400',
        color: colors.black,
    },
   

    bodyInput: {
        fontFamily: fonts.primary_regular_font,
        fontSize: 14,
        fontWeight: '400',
    },
    textinputheadline1: {
        fontFamily: fonts.primary_medium_font,
        fontSize: 18,
        fontWeight: '500',
    },
    textinputheadline2: {
        fontFamily: fonts.primary_bold_font,
        fontSize: 18,
        fontWeight: '700',
    },
})
