import React from 'react'
import { Text, View, StyleSheet, } from 'react-native'


import { textstyles } from './textstyles';
import { STRING_CONSTANTS } from '../../utils';


const AppText = ({
    type,
    text,
    customStyle,
    isMultiline,
    numberOfLines,
}) => {

    if (type == STRING_CONSTANTS.textConstants.LABEL)
        return <Text style={{ ...textstyles.labelStyle, ...customStyle }}>{text}</Text>
    else if (type == STRING_CONSTANTS.textConstants.HEADING)
        return <Text style={{ ...textstyles.heading, ...customStyle }}>{text}</Text>
    
    
    else if (type == STRING_CONSTANTS.textConstants.HEADLINE1)
        return <Text style={{ ...textstyles.headline1, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.HEADLINE2)
        return <Text style={{ ...textstyles.headline2, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.HEADLINE3)
        return <Text style={{ ...textstyles.headline3, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.SUBHEADLINE1)
        return <Text style={{ ...textstyles.subHeadline1, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.SUBHEADLINE2)
        return <Text style={{ ...textstyles.subHeadline2, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.SUBHEADLINE3)
        return <Text style={{ ...textstyles.subHeadline3, ...customStyle }}>{text}</Text>
    
    
    
    else if (type == STRING_CONSTANTS.textConstants.TITLE1)
        return <Text style={{ ...textstyles.title1, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.TITLE2)
        return <Text style={{ ...textstyles.title2, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.TITLE3)
        return <Text style={{ ...textstyles.title3, ...customStyle }}>{text}</Text>
    
    
    else if (type == STRING_CONSTANTS.textConstants.CAPTION1)
        return <Text numberOfLines={numberOfLines} style={{ ...textstyles.caption1, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.CAPTION2)
        return <Text numberOfLines={numberOfLines} style={{ ...textstyles.caption2, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.CAPTION3)
        return <Text numberOfLines={numberOfLines} style={{ ...textstyles.caption3, ...customStyle }}>{text}</Text>
    


    else if (type == STRING_CONSTANTS.textConstants.ERROR)
        return <Text style={{ ...textstyles.error, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.BODY)
        return <Text style={{ ...textstyles.body, ...customStyle }}>{text}</Text>
    
    else if (type == STRING_CONSTANTS.textConstants.TINY)
        return <Text style={{ ...textstyles.tiny, ...customStyle }}>{text}</Text>
    
    




    else
        return <Text style={{ ...textstyles.defaultStyles, ...customStyle }}>{text}</Text>
}

export default AppText