import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { colors } from '../../utils'
import CustomIcon from '../CustomIcon'

const BackButton = ({ onPress, backButtonColor}) => {

    const navigation = useNavigation()

    return (
        <>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={() => {navigation.goBack()}}
                style={[styles.container, {backgroundColor: backButtonColor ? backButtonColor : null} ]}
                >
                <CustomIcon
                    iconName="chevron-left"
                    iconType="Feather"
                    iconColor={colors.white}
                />
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 36,
        width: 36,
        borderRadius: 18,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: colors.gray,
    },
})

export default BackButton
