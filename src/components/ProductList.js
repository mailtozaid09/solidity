import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { STRING_CONSTANTS, colors } from '../utils'
import CardTitle from './CardTitle'
import ProductCategories from './ProductCategories'
import { product_data } from '../global/sampleData'
import ProductCard from './ProductCard'


const ProductList = ({ title, isSeeAll, onPress, }) => {

    const [activeCategory, setActiveCategory] = useState('All');

    return (
        <View style={styles.container} >
            <CardTitle
                title="Most Popular"
                isSeeAll={true}
                onPress={() => {}}
            />

            <ProductCategories 
                activeCategory={activeCategory}
                setActiveCategory={(val) => {setActiveCategory(val)}}
            /> 


            <FlatList
                data={product_data}
                keyExtractor={item => item.title}
                contentContainerStyle={{marginVertical: 20, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between'}}
                renderItem={({item, index}) => (
                    <ProductCard
                        key={index}
                        index={index}
                        item={item}
                    />
                )}
            />

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
    }
})

export default ProductList