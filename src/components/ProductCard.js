import React, { useState } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity,} from 'react-native'
import { STRING_CONSTANTS, colors } from '../utils'
import Icon from '../utils/icons'
import AppText from './text'
import { screenWidth } from '../global/constants'
import { media } from '../assets/media'
import CustomIcon from './CustomIcon'


const ProductCard = ({ item }) => {

    return (
        <TouchableOpacity 
            activeOpacity={0.5}
            onPress={() => {}}
            style={styles.container}
        >
            <View>
                <Image source={item?.image} style={styles.image} />
                <CustomIcon
                    iconName="hearto"
                    iconType="AntDesign"
                    iconColor={colors.primary}
                    iconSize={14}
                    iconContainer={styles.wishlistContainer}
                    onPress={() => {}}
                />
            </View>
            <View style={styles.content} >
                <AppText
                    text={item?.price}
                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                    customStyle={{color: colors.black}}
                />
                
                <AppText
                    text={item?.name}
                    numberOfLines={1}
                    type={STRING_CONSTANTS.textConstants.CAPTION2}
                    customStyle={{color: colors.black}}
                />
                
                <AppText
                    text={item?.description}
                    numberOfLines={2}
                    type={STRING_CONSTANTS.textConstants.CAPTION3}
                    customStyle={{color: colors.black}}
                />
            
            </View>
           
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    container: {
        width: screenWidth/2-25,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.light_gray,
        marginBottom: 14,
    },
    image: {
        width: screenWidth/2-25,
        height: screenWidth/2,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    content: {
        padding: 10,
    },
    wishlistContainer: { 
        height: 28, 
        width: 28,  
        borderRadius: 14,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 10,
        top: 10,
    }
})

export default ProductCard