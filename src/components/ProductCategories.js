import React, { useState, } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, FlatList,} from 'react-native'
import { STRING_CONSTANTS, colors } from '../utils'
import AppText from './text'
import { product_categories } from '../global/sampleData'


const ProductCategories = ({ activeCategory, setActiveCategory }) => {

    return (
        <View style={styles.container} >
            <FlatList
                data={product_categories}
                keyExtractor={item => item.title}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({item, index}) => (
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        onPress={() => setActiveCategory(item?.title)}
                        style={[styles.categoryContainer, item?.title == activeCategory ? styles.activeStyle : styles.inActiveStyle ]} >
                        <AppText
                            text={item?.title}
                            type={STRING_CONSTANTS.textConstants.CAPTION2}
                            customStyle={{marginTop: 2, textAlign: 'center', color: item?.title == activeCategory ? colors.white : colors.black}}
                        />
                    </TouchableOpacity>
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    categoryContainer: {
        height: 40,
        minWidth: 80,
        paddingHorizontal: 20,
        marginRight: 10,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    activeStyle: {
        borderWidth: 0.5,
        borderColor: colors.primary,
        backgroundColor: colors.primary,
    },
    inActiveStyle: {
        borderWidth: 0.5,
        borderColor: '#A3A3A3',
        backgroundColor: colors.white,
    }
})

export default ProductCategories