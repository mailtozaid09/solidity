import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity,} from 'react-native'
import { colors } from '../utils'
import Icon from '../utils/icons'


const CustomIcon = ({ onPress, iconContainer, iconName, iconSize, iconType, iconColor }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={onPress}    
            style={[styles.container, iconContainer]} 
        >
            <Icon type={iconType || AntDesign} name={iconName || 'close'} size={iconSize || 20} color={iconColor || colors.black} />
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    container: {
        
    }
})

export default CustomIcon