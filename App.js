
import React, { useEffect } from 'react'
import { LogBox, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';

import Navigator from './src/navigator';
import SplashScreen from 'react-native-splash-screen';

LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        console.log('App mounted, hiding splash screen');
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])

    return (
        <NavigationContainer>
            <Navigator />
        </NavigationContainer>
    )
}

export default App